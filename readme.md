
read this on log structured storage

http://blog.notdot.net/2009/12/Damn-Cool-Algorithms-Log-structured-storage


Implemented a super simple version in python

- index is stored in a python dictionary
- strings are written like this
  - 's' - 1 byte indicator
  - 4s - 4 bytes (ascii string)
  - (variable)s - the actual key (size is the element above)
  - 4s - 4 bytes (ascii string)
  - (variable)s - the actual value

- in the index the strings are stored with key as the key and the
file position and value as the value in the dictionary.

TODO:
- change the rebuild_index to deal with a random index block
- the rebuild really should do something slightly different
  - seek to end of the file
  - try to find the last index block
  - initialize the index with what you found
  - then read through the file forward from the last good index
  - should be done
-
- did not write out fixed size blocks
  - thought that would suck for just string key values
