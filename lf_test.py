import traceback, struct, random, string

class LF:
    def __init__(self,filename="lf.db"):

        # not sure about this, the logic might be
        # wrong here
        try:
            self.outf = open(filename,"r+b")
        except IOError:
            self.outf = open(filename, "w+b")

        # cp is the current position in the file
        self.cp = 0

        # this is the index,
        # this file only holds string key value pairs
        self.index = {}

        # when we write an index record we update this
        self.index_position = 0

        self.write_pointer()


    def get_string(self, key):
        # meh
        if key in self.index:
            return self.index[key]["value"]
        return None

    def display_pointer(self):
        self.outf.seek(0)
        record_indicator = self.outf.read(1)
        if record_indicator != "p":
            print("WARNING: pointer record not present")
            return
        pointed_to_index = struct.unpack("Q", self.outf.read(8))
        print("INFO: pointer value", pointed_to_index)
        self.outf.seek(0,2) # jump to end of file again

    def write_pointer(self):

        # save off the current file position
        current_pos = self.outf.tell()

        # go to the front of the file
        self.outf.seek(0)
        self.outf.write("p")
        self.outf.write(struct.pack("Q",self.index_position))
        self.outf.seek(current_pos)

    def set_string(self, key, value):
        print("set_string",key,value)

        # save off the starting position
        start_pos = self.cp

        # record indicator (s == string ) derp
        self.outf.write("s")
        self.cp = self.cp + 1

        # fancy code not used
        # write the len (4 bytes)
        # self.outf.write(struct.pack("I",len(key)))
        # ended up using 4 ascii bytes
        # this limits string size to 9999
        # good enough for my purposes
        # and easier to debug
        self.outf.write("{0:04}".format(len(key)))
        self.cp = self.cp + 4

        # write the key
        self.outf.write(key)
        self.cp = self.cp + len(key)

        # write the len (4 bytes)
        # self.outf.write(struct.pack("I",len(value)))
        self.outf.write("{0:04}".format(len(value)))
        self.cp = self.cp + 4

        # write the value
        self.outf.write(value)
        self.cp = self.cp + len(value)

        # update the index overwrite anything already there
        self.index[key] = { "value" : value, "cp" : start_pos }

    def __repr__(self):
        return "cp = {0} {1}".format(self.cp, self.index)

    def write_index(self):
        # save off the current position...
        self.index_position = self.outf.tell()

        print("write index record")
        self.outf.write("i") # record indicator i
        keys = self.index.keys()

        # total number of keys
        self.outf.write("{0:04}".format(len(keys)))

        for k in keys:
            v = self.index[k]

            # write the key len
            self.outf.write("{0:04}".format(len(k)))
            # write the key
            self.outf.write(k)
            # write the pointer
            self.outf.write("{0:04}".format(v["cp"]))

        # update the pointer record at the front of the file
        self.write_pointer()

    def rebuild_index(self):
        """
        rebuild index in memory
        """
        print("rebuilding index...")
        self.outf.seek(0)
        self.index = {}

        while True:
            cur_pos = self.outf.tell()
            print("rebuild","cur_pos",cur_pos)

            # record indicator is really always s currently
            record_indicator = self.outf.read(1)
            if record_indicator == "":
                break

            # skip pointer
            if record_indicator == "p":
                print("INFO: skipping pointer")
                pointer_to_index = self.outf.read(8)

                print("INFO: pointer is at", pointer_to_index)
                continue

            # skipping the index records
            if record_indicator == "i":
                print("INFO: skipping index records...")
                key_count = int(self.outf.read(4))
                for i in range(key_count):
                    key_size = self.outf.read(4)
                    key_size = int(key_size)
                    key = self.outf.read(key_size)
                    file_pos = self.outf.read(4)

                print("finished skipping index record")
                continue


            if record_indicator != "s":
                print("WARNING: invalid record indicator")




            # read 4 bytes and cast to int
            ll = self.outf.read(4)
            print("rebuild ll1",ll)
            # ll = struct.unpack("I",ll)
            ll = int(ll)
            print("rebuild ll1 unpacked",ll)

            # fancy code
            # struct.unpack returns a tuple
            # k = self.outf.read(ll[0])
            # read bytes for the correct length
            k = self.outf.read(ll)
            print("rebuild k",k)
            ll = self.outf.read(4)
            print("rebuild ll2",ll)
            # ll = struct.unpack("I",ll)
            ll = int(ll)
            print("rebuild ll2 unpacked",ll)
            # v = self.outf.read(ll[0])
            v = self.outf.read(ll)
            print("rebuild v",v)

            print("rebuild",k)
            self.index[k] = { "value" : v, "cp" : cur_pos }

        self.outf.seek(0,2) # seek to end of file

        # save off the file position
        self.cp = self.outf.tell()
        print("finished rebuilding index")

    def close(self):
        self.outf.close()

def test_simple_string():
    """
    test inserting 3 strings
    """
    lf = None
    try:
        lf = LF()
        lf.set_string("a", "tony")
        lf.set_string("b", "beth")
        lf.set_string("a", "bob")
        print(lf)
    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

def test_rebuild_index():
    """
    open the file and rebuild the index
    """
    lf = None
    try:
        lf = LF()
        lf.rebuild_index()
        print("REBUILT",lf)
    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

def simple_string_test():
    test_simple_string()
    test_rebuild_index()

def random_test():
    lf = None
    try:
        lf = LF()
        for i in range(1000):
            N = 5
            key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
            N = 10
            val = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
            lf.set_string(key,val)
    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

def test_write_index():
    lf = None
    try:
        lf = LF()
        lf.set_string("a","tony")
        lf.write_index()
    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

def test_write_index_then_rebuild():
    lf = None
    try:
        lf = LF()
        lf.set_string("a","tony")
        lf.write_index()
        lf.rebuild_index()
        print(lf)
    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

def test_pointer():
    lf = None
    try:
        lf = LF()
        lf.set_string("a","tony")
        lf.rebuild_index()
        lf.write_index()
        lf.write_pointer()
        lf.display_pointer()

    except:
        traceback.print_exc()
    finally:
        if lf is not None:
            lf.close()

# simple_string_test()

# random_test()
# test_rebuild_index()

# test_write_index()
# test_write_index_then_rebuild()
test_pointer()
